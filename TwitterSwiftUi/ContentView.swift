//
//  ContentView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct ContentView: View {
    @State var openSheets: Bool = false
    var body: some View {
        ZStack (alignment: .top){
            MainTabView()
        }
        .fullScreenCover(isPresented: $openSheets) {
            NavigationView {
                ProfileMenuView(fData: fallowersData.first!)
                    .navigationBarHidden(true)
            }
            
         }
//        .navigationTitle("Home")
//        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                Button {
                    openSheets.toggle()
                } label: {
                    Circle()
                        .frame(width: 33, height: 33)
                }
            }
            ToolbarItem(placement: .navigationBarTrailing) {
                NavigationLink {
                    InfoPageView()
                        .navigationBarHidden(true)
                } label: {
                    Image("bird")
                        .resizable()
                        .frame(width: 33, height: 33)
                }
            }
        
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
