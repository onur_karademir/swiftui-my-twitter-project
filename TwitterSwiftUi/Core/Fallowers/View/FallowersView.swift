//
//  FallowersView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 18.08.2022.
//

import SwiftUI

struct FallowersModel : Identifiable {
    
    var id = UUID()
    var fallowersColor : Color
    var fallowersName : String
    var fallowersNick : String
    var fallowersDesc : String
    
}

let fallowersData = [

    FallowersModel(fallowersColor: .purple, fallowersName: "Nick O.", fallowersNick: "@nick", fallowersDesc: "I am IOS developer."),
    
    FallowersModel(fallowersColor: .mint, fallowersName: "Arda Guler", fallowersNick: "@arda10", fallowersDesc: "Sadece Fenerbahce ⭐️⚽️"),
    
    FallowersModel(fallowersColor: .yellow, fallowersName: "Luise", fallowersNick: "@lui", fallowersDesc: "Not happy..."),
    
    FallowersModel(fallowersColor: .blue, fallowersName: "Devid", fallowersNick: "@david", fallowersDesc: "Lorem Ipsum.."),
    
    FallowersModel(fallowersColor: .red, fallowersName: "Maxi Lopez", fallowersNick: "@MLopez", fallowersDesc: "Dilor Set..."),
    
    FallowersModel(fallowersColor: .gray, fallowersName: "Mario", fallowersNick: "@mario", fallowersDesc: "i love pipe 🦸🏻‍♂️")
]

struct FallowersView: View {
    var fData : FallowersModel
    var body: some View {
        ScrollView {
            LazyVStack {
                ForEach(fallowersData, id:\.id) { item in
                    FallowersRowView(Fdata: item)
                }
            }
        }
    }
}

struct FallowersView_Previews: PreviewProvider {
    static var previews: some View {
        FallowersView(fData: fallowersData.first!)
    }
}
