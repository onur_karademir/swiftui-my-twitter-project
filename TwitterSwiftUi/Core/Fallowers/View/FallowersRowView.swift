//
//  FallowersRowView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 18.08.2022.
//

import SwiftUI

struct FallowersRowView: View {
    var Fdata : FallowersModel
    @State var isFallow : Bool = false
    var body: some View {
        VStack (alignment: .leading){
            HStack (alignment: .center, spacing: 20){
                Circle()
                    .frame(width: 55, height: 55)
                    .foregroundColor(Fdata.fallowersColor)
                VStack (alignment:.leading, spacing: 8){
                    Text(Fdata.fallowersName)
                        .font(.subheadline).bold()
                        .foregroundColor(.black)
                    Text(Fdata.fallowersNick)
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                
                Spacer()
                
                Button {
                    isFallow.toggle()
                } label: {
                    Text(isFallow ? "Unfallow" : "Fallow")
                        .font(.subheadline)
                        .foregroundColor(isFallow ? .white : .blue)
                        .frame(width: 70)
                        .padding(.vertical,9)
                        .padding(.horizontal,9)
                        .background(isFallow ? .blue : .gray.opacity(0.1))
                        .clipShape(Capsule())
                }

            }
            HStack{
                Text(Fdata.fallowersDesc)
                    .font(.title3)
                    .foregroundColor(.gray)
            }
            .padding(.leading,5)
            .padding(.top,5)
            Divider()
        }.padding()
    }
}

struct FallowersRowView_Previews: PreviewProvider {
    static var previews: some View {
        FallowersRowView(Fdata: fallowersData.first!)
    }
}
