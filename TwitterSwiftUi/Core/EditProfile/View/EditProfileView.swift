//
//  EditProfileView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 20.08.2022.
//

import SwiftUI

struct EditProfileView: View {
    @Environment (\.presentationMode) var presentationMode
    
    @StateObject var editProfileViewModelStateObj : EditProfileViewModel = EditProfileViewModel()
    
    var body: some View {
        
        NavigationView {
            ScrollView {
                EditProfileHeaderView(headerString: "Profile Edit.", headerImage: "bird")
                
                ForEach(editProfileViewModelStateObj.editProfileViewModel,id:\.id) { item in
                    EditProfileTextAreaView(key: item.key, value: item.value)
                }
            }
            
            .navigationTitle("Edit Profile")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(
                leading:
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        BackButtonView(backButtonString: "arrow.left")
                    }
            
            )
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}
