//
//  FeedView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct FeedView: View {
    @State var openSheets: Bool = false
    var body: some View {
        ZStack (alignment: .bottomTrailing){
            ScrollView {
                LazyVStack {
                    ForEach(dataUser, id: \.id) { item in
                        TweetRowView(data: item)
                    }
                }
            }
            Button {
                openSheets.toggle()
            } label: {
                TweetButtonView(tweetButtonName: "highlighter")
            }
            .background(Color.blue)
            .clipShape(Circle())
            .foregroundColor(.white)
            .padding()
            .sheet(isPresented: $openSheets) {
                NewTweetView()
             }
        }
    }
}

struct FeedView_Previews: PreviewProvider {
    static var previews: some View {
        FeedView()
    }
}
