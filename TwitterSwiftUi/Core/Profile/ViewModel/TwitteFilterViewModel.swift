//
//  TwitteFilterViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import Foundation

enum TweetFilterViewModel: Int, CaseIterable {
    case twitts
    case raplies
    case like
    
    var title: String {
        switch self {
        case.twitts : return "Tweets"
        case.raplies : return "Raplies"
        case.like : return "Likes"
        }
    }
}
