//
//  ProfileView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct ProfileView: View {
    //appdata
    @AppStorage("current_user_profile") var currentUserProfile : Bool = false
    @AppStorage("user_name") var currentUserName: String?
    @AppStorage("user_full_name") var currentUserFullName: String?
    //

    @State var selectedFilter : TweetFilterViewModel = .twitts
    @Environment (\.presentationMode) var presentationMode
    var data : userDataModel
    var fData : FallowersModel
    
    @State var isLike : Bool = false
    @State var isRetweet : Bool = false
    @State var isBookmark : Bool = false
    @State var openSheets: Bool = false

    var body: some View {
        VStack (alignment: .leading) {
            headerView
            
            actionButtons
            
            userInfoWiew
            
            tweetFilterBar
            
            tweetsView

            Spacer()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(data: dataUser.first!, fData: fallowersData.first!)
    }
}

extension ProfileView {
    
    var headerView : some View {
        
        ZStack (alignment: .bottomLeading){
            Color(.systemBlue).ignoresSafeArea()
            
            VStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                    currentUserProfile = false
                } label: {
                    Image(systemName: "arrow.left")
                        .resizable()
                        .frame(width: 25, height: 20)
                        .foregroundColor(.white)
                        .offset(x: 15, y: 15)
                }

                Circle()
                    .frame(width: 75, height: 75)
                    .foregroundColor(currentUserProfile ? .black : data.userColor)
                    .offset(x: 15, y: 25)
            }
        }
        .frame(height: 120)

    }
    
    var actionButtons: some View {
        
        HStack (spacing: 20) {
            Spacer()
            Image(systemName: "bell.badge")
                .foregroundColor(Color.black)
                .font(.title3)
                .padding(6)
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))
            NavigationLink {
                EditProfileView()
                    .navigationBarHidden(true)
            } label: {
                Text("Edit Profile")
                    .fontWeight(.bold)
                    .font(.subheadline)
                    .frame(width: 120, height: 35)
                    .foregroundColor(Color.black)
                    .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color.gray))
            }

//            Button {
//                //
//            } label: {
//                Text("Edit Profile")
//                    .fontWeight(.bold)
//                    .font(.subheadline)
//                    .frame(width: 120, height: 35)
//                    .foregroundColor(Color.black)
//                    .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color.gray))
//            }

        }
        .padding(.trailing)
    }
    
    var userInfoWiew : some View {

        VStack(alignment: .leading, spacing: 4) {
            HStack {
                Text(currentUserProfile ? currentUserFullName ?? "" : data.userName)
                    .fontWeight(.bold)
                    .font(.title2)
                Image(systemName: "checkmark.seal.fill")
                    .foregroundColor(Color.blue)
            }
            Text(currentUserProfile ? currentUserName ?? "" :data.userNick)
                .font(.subheadline)
                .foregroundColor(.gray)
            Text(data.userTitle)
                .font(.title3)
                .fontWeight(.semibold)
                .padding(.vertical)
            
            HStack (spacing: 30){
                HStack{
                    Image(systemName: "mappin.and.ellipse")
                    Text(data.userLocation)
                }
                HStack{
                    Image(systemName: "link")
                    Text(data.userWebSite)
                }
            }
            
            .foregroundColor(.gray)
            .font(.caption)
            
            HStack (spacing: 25){
                HStack (spacing: 5) {
                    NavigationLink {
                        FallowingView()
                            .navigationBarTitleDisplayMode(.inline)
                            .navigationTitle("Fallowing")
                    } label: {
                        Text("2")
                            .fontWeight(.bold)
                            .foregroundColor(.black)
                        Text("Fallowing")
                            .font(.caption)
                            .foregroundColor(Color.gray)
                    }
                }
                HStack{
                    NavigationLink {
                        FallowersView(fData: fData)
                            .navigationBarTitleDisplayMode(.inline)
                            .navigationTitle("Fallowers")
                    } label: {
                        Text("6.9M")
                            .foregroundColor(.black)
                            .fontWeight(.bold)
                    }
                    Text("Fallowers")
                        .font(.caption)
                        .foregroundColor(Color.gray)
                }
            }
            .padding(.vertical)
        }
        .padding(.horizontal)
    }
    
    var tweetFilterBar : some View {
        
        HStack {
            ForEach(TweetFilterViewModel.allCases, id:\.rawValue) { item in
                VStack {
                    Text(item.title)
                        .font(.subheadline)
                        .fontWeight(selectedFilter == item ? .semibold : .regular)
                        .foregroundColor(selectedFilter == item ? .black : .gray)
                    
                    if selectedFilter == item {
                        Capsule()
                            .foregroundColor(.blue)
                            .frame(height:3)
                    }else {
                        Capsule()
                            .foregroundColor(.clear)
                            .frame(height:3)
                    }
                }
                .onTapGesture {
                    withAnimation (.easeInOut){
                        self.selectedFilter = item
                    }
                }
            }
        }
        .overlay(Divider().offset(x:0, y:15))
    }
    
    var tweetsView : some View {
        
        ScrollView {
            LazyVStack {
                if currentUserProfile {
                    ForEach(dataUser, id: \.id) { item in
                        VStack (alignment: .leading){
                            //profile image and info
                            HStack (alignment: .top, spacing: 12){
                                //user image container
                                Circle()
                                    .frame(width: 56, height: 56)
                                    .foregroundColor(.black)
                                
                                VStack (alignment: .leading, spacing: 4){
                                    //user info and user name
                                    HStack {
                                        Text(currentUserFullName ?? "")
                                            .font(.headline).bold()
                                        Text(currentUserName ?? "")
                                            .foregroundColor(.gray)
                                            .font(.caption)
                                        Text("2w")
                                            .foregroundColor(.gray)
                                            .font(.caption)
                                    }
                                    //tweet
                                    Text("Tweet \(item.id)")
                                        .font(.subheadline)
                                        .multilineTextAlignment(.leading)
                                }
                                
                            }
                            //action buttons
                            
                            HStack {
                                Button {
                                    openSheets.toggle()
                                } label: {
                                    Image(systemName: "bubble.left")
                                        .font(.subheadline)
                                }
                                Spacer()
                                Button {
                                    isRetweet.toggle()
                                } label: {
                                    Image(systemName: "arrow.2.squarepath")
                                        .font(.subheadline)
                                        .foregroundColor(isRetweet ? .blue : .gray)
                                }
                                Spacer()
                                Button {
                                    isLike.toggle()
                                } label: {
                                    Image(systemName: isLike ? "heart.fill" : "heart")
                                        .font(.subheadline)
                                        .foregroundColor(isLike ? .red : .gray)
                                }
                                Spacer()
                                Button {
                                    isBookmark.toggle()
                                } label: {
                                    Image(systemName: isBookmark ? "bookmark.fill" : "bookmark")
                                        .font(.subheadline)
                                        .foregroundColor(isBookmark ? .blue : .gray)
                                }

                            }
                            .padding()
                            .foregroundColor(.gray)
                            Divider()
                            //fullScreenCover (tam ekran sheets)
                                .sheet(isPresented: $openSheets) {
                                    NewTweetView()
                                 }
                        }
                        .padding()
                    }
                }else {
                    ForEach(dataUser, id: \.id) { item in
                        TweetRowView(data: data)
                    }
                }
//                ForEach(dataUser, id: \.id) { item in
//                    TweetRowView(data: data)
//                }
            }
        }
    }
}
