//
//  InfoPageDetailHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct InfoPageDetailHeaderView: View {
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        HStack (alignment: .top){
            HeaderBirdView(headerPic: "bird")
            Spacer()
            VStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    HeaderXmarkButtonView(headerButtonString: "xmark")
                }
                
            }
        }
        .padding(.top)
        .padding(.horizontal)

    }
}

struct InfoPageDetailHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageDetailHeaderView()
    }
}
