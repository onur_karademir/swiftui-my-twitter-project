//
//  InfoPageLazyVstackView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct InfoPageLazyVstackView: View {
    let infoaPageLazyVstackImage : String
    var body: some View {
        LazyVStack {
            ForEach(0..<10) { index in
                Circle()
                    .fill(Color.mint)
                    .frame(width: 155, height: 155, alignment: .center)
                    .frame(
                          minWidth: 0,
                          maxWidth: .infinity,
                          minHeight: 300,
                          maxHeight: .infinity
                        )
                    .overlay(
                        Image(infoaPageLazyVstackImage)
                            .resizable()
                            .frame(width: 60, height: 60, alignment: .center)
                    )
                    .background(
                       Rectangle()
                        .fill(Color.blue)
                    )
            }
        }
    }
}

struct InfoPageLazyVstackView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageLazyVstackView(infoaPageLazyVstackImage: "dove")
    }
}
