//
//  InfoDetailPageTopHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct InfoDetailPageTopHeaderView: View {
    let headerDetailTitle : String
    let headerDetailSubTitle : String
    var body: some View {
        VStack (alignment: .leading){
            VStack (alignment: .leading){
                HStack (alignment: .top, spacing: 30){
                    Circle()
                        .stroke(.white, lineWidth: 3)
                        .frame(width: 69, height: 69, alignment: .center)
                        .background(
                            Image("parrot")
                                .resizable()
                                .frame(width: 45, height: 45, alignment: .center)
                        )
                    VStack (alignment: .leading, spacing: 5){
                        Text(headerDetailTitle)
                            .font(.title)
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                        Text(headerDetailSubTitle)
                            .font(.title3)
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                    }
                    VStack {
                        HStack (spacing: 9){
                            Image(systemName: "info.circle.fill")
                                .foregroundColor(.white)
                            Image(systemName: "checkmark.seal")
                                .foregroundColor(.white)
                            Image(systemName: "mustache.fill")
                                .foregroundColor(.white)
                            Image(systemName: "checkerboard.shield")
                                .foregroundColor(.white)
                        }
                        .padding(.top, 6)
                        
                        HStack (spacing: 9){
                            Image(systemName: "checkerboard.shield")
                                .foregroundColor(.white)
                            Image(systemName: "mustache.fill")
                                .foregroundColor(.white)
                            Image(systemName: "checkmark.seal")
                                .foregroundColor(.white)
                            Image(systemName: "info.circle.fill")
                                .foregroundColor(.white)
                        }
                        .padding(.top, 6)
                    }
                    Spacer()
                }
            }
            .padding()
            .background(.blue)
        }
    }
}

struct InfoDetailPageTopHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        InfoDetailPageTopHeaderView(headerDetailTitle: "title", headerDetailSubTitle: "sub")
    }
}
