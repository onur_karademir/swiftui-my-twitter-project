//
//  InfoPageDetailView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 26.08.2022.
//

import SwiftUI

struct InfoPageDetailView: View {
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack(alignment: .leading, spacing: 34) {
            VStack (alignment: .leading) {
                InfoPageDetailHeaderView()
            }
            
            VStack {
                InfoDetailPageTopHeaderView(headerDetailTitle: "Info.", headerDetailSubTitle: "Detail.")
            }
            
            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack (spacing: 30){
                    ScrollView (.horizontal, showsIndicators: false) {
                        InfoPageLazyHstackView(lazyHstackImage: "bird")
                    }
                    InfoPageLazyVstackView(infoaPageLazyVstackImage: "parrot")
                }
            }

        }
    }
}

struct InfoPageDetailView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageDetailView()
    }
}
