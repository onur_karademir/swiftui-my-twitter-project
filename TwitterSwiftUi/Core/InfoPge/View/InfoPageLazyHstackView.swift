//
//  InfoPageLazyHstackView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct InfoPageLazyHstackView: View {
    let lazyHstackImage : String
    var body: some View {
        LazyHStack {
            ForEach(0..<10) { index in
                Image(lazyHstackImage)
                    .resizable()
                    .frame(width: 55, height: 55, alignment: .center)
                    .frame(
                          minWidth: 220,
                          maxWidth: .infinity,
                          minHeight: 270,
                          maxHeight: .infinity
                        )
                    
                    .background(
                       Rectangle()
                        .fill(Color.white)
                        .cornerRadius(25)
                        .shadow(radius: 10)
                        .padding([.leading, .trailing], 10)
                        .padding(.vertical, 5)
                    )
            }
        }
    }
}

struct InfoPageLazyHstackView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageLazyHstackView(lazyHstackImage: "bird")
    }
}
