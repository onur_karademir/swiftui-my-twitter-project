//
//  InfoPageView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 26.08.2022.
//

import SwiftUI

struct InfoPageView: View {
    @Environment (\.presentationMode) var presentationMode
    @State var openInfoSheets : Bool = false
    var body: some View {
        ZStack (alignment: .topLeading){
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                BackButtonView(backButtonString: "arrow.left")
            }
            VStack (spacing: 20){
                Spacer()
                AuthenticationHeaderView(titleText: "Twitter Info Page.", subtitleText: "Info page. ", image: "bird")
                Button {
                    //actions
                    openInfoSheets.toggle()
                } label: {
                    ClasicButtonView(clasicButtonText: "Info", clasicButtonString: "info")
                }
                VStack {
                    TextGeneralView(textString: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.")
                }
                Spacer()
                Spacer()
            }
            .sheet(isPresented: $openInfoSheets) {
                InfoPageDetailView()
             }
        }
        .padding(.horizontal)
    }
}

struct InfoPageView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageView()
    }
}
