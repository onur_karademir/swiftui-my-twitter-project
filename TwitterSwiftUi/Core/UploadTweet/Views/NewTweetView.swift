//
//  NewTweetView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 12.08.2022.
//

import SwiftUI

struct NewTweetView: View {
    @State var textFieldData: String = ""
    @State var textFieldTweet : String = ""
    @FocusState private var isFocused : Bool
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            newTweetViewHeader
            newTweetBody
            VStack {
                Text(textFieldTweet)
                    .frame(height: 100)
            }
            .padding()
            Spacer()
        }
        .padding()
    }
}

struct NewTweetView_Previews: PreviewProvider {
    static var previews: some View {
        NewTweetView()
    }
}

extension NewTweetView {
    
    var newTweetViewHeader : some View {
        
        HStack {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Cancel")
                    .foregroundColor(Color(.systemBlue))
                    .bold()
            }
            Spacer()
            
            Button {
                isFocused = true
                textFieldTweet = textFieldData
                textFieldData = ""
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Tweet")
                    .foregroundColor(Color.white)
                    .bold()
                    .padding(.vertical, 9)
                    .padding(.horizontal)
                    .background(Color(.systemBlue))
                    .clipShape(Capsule())
            }
        }

    }
    
    var newTweetBody : some View {
        HStack {
            Circle()
                .frame(width: 50, height: 50)
            TextField("Something is happening ...", text: $textFieldData)
                .focused($isFocused)
                .padding()
                .font(.headline)
                .foregroundColor(.black)
                .background(Color.white)
                .cornerRadius(50)
            .border(Color.secondary, width: 0.5)
        }
        .padding(.vertical,20)
    }
}
