//
//  MainTabView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI
enum Tabs : String {
    case home
    case explore
    case notify
    case messages
}
struct MainTabView: View {
    @State var selectedIndex : Tabs = .home
    var body: some View {
        TabView(selection: $selectedIndex) {
            //tab 0
            FeedView()
                .onTapGesture {
                    self.selectedIndex = .home
                }
                .tabItem {
                Image(systemName: "house")
                }.tag(Tabs.home)
            
            //tab 1
            
           ExploreView()
                .onTapGesture {
                    self.selectedIndex = .explore
                }
                .tabItem {
                Image(systemName: "magnifyingglass")
                }.tag(Tabs.explore)
            
            //tab 2
            
            NotifView()
                .onTapGesture {
                    self.selectedIndex = .notify
                }
                .tabItem {
                Image(systemName: "bell")
                }.tag(Tabs.notify)
                .badge(5)
            
            //tab 3
            
            MessagesView()
                .onTapGesture {
                    self.selectedIndex = .messages
                }
                .tabItem {
                Image(systemName: "envelope")
                }.tag(Tabs.messages)
                .badge(3)
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle(selectedIndex.rawValue.capitalized)
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
