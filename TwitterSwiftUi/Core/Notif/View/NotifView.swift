//
//  NotifView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct NotifView: View {
    var body: some View {
        VStack {
            NotifMenuView()
            VStack {
                
                ScrollView {
                    VStack (alignment: .leading, spacing: 15){
                        ForEach(NotifViewModel.allCases, id:\.rawValue) { item in
                            
                            NotifIconView(iconName: "star.fill", iconImageColor: item.color, userIconColor: item.color)
                            
                            NotifBodyView(personNotifName: item.person, personNotifDetail: item.notif_detail, personNotif: item.notif)
                            
                            Divider()
                        }
                    }
                    .padding(.horizontal)
                }
            }
        }
    }
}

struct NotifView_Previews: PreviewProvider {
    static var previews: some View {
        NotifView()
    }
}
