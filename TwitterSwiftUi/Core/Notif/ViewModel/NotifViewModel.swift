//
//  NotifViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 14.08.2022.
//

import Foundation
import SwiftUI

enum NotifViewModel : Int , CaseIterable {
    case notifOne
    case notifTwo
    case notifThree
    case notifFour
    
    var person : String {
        
        switch self {
        case .notifOne : return "Eksi Sozluk"
        case .notifTwo : return "kaliteli izleme rehberi"
        case .notifThree : return "Hasan Yalcin"
        case .notifFour : return "gunluk feyyaz yigit dozu"
        }
        
    }
    var notif_detail : String {
        
        switch self {
        case .notifOne : return "adli kullnaicinin yeni tweeti"
        case .notifTwo : return "Capamag adli kullanicinin bir videsunu Retweetledi"
        case .notifThree : return "Capamag adli kullanicinin bir tweetini Retweetledi"
        case .notifFour : return "adli kullnaicinin yeni tweeti"
        }
        
    }
    
    var notif : String {
        
        switch self {
        case .notifOne: return "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        case .notifTwo: return "type specimen book. It has survived not only five centuries, but also the leap"
        case .notifThree: return "tly with desktop publishing software like Aldus PageMaker including versions"
        case .notifFour: return "tly with desktop publishing software like Aldus PageMaker including versions"
        }
    }
    
    var color : Color {
        
        switch self {
        case .notifOne : return .green
        case .notifTwo : return .blue
        case .notifThree : return .mint
        case .notifFour : return .purple
        }
        
    }
    
}
