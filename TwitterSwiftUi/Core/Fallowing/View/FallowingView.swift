//
//  FallowingView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 20.08.2022.
//

import SwiftUI

struct FallowingModel : Identifiable {
    let id = UUID()
    var fallowinngColor : Color
    var fallowinngName : String
    var fallowinngNick : String
    var fallowinngDesc : String
}

var fallowingData = [
    FallowingModel(fallowinngColor: .yellow, fallowinngName: "Arda Guler", fallowinngNick: "@arda10", fallowinngDesc: "Sadece Fenerbahce ⭐️⚽️"),
    FallowingModel(fallowinngColor: .blue, fallowinngName: "Alex D.", fallowinngNick: "@alex10", fallowinngDesc: "I Love Fenerbahce ⭐️⚽️")
]

struct FallowingView: View {
    var body: some View {
        ScrollView {
            LazyVStack {
                ForEach(fallowingData, id:\.id) { item in
                    FallowingRowView(fallowingData: item)
                }
            }
        }
    }
}

struct FallowingView_Previews: PreviewProvider {
    static var previews: some View {
        FallowingView()
    }
}
