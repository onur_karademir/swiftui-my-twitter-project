//
//  FallowingRowView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 20.08.2022.
//

import SwiftUI

struct FallowingRowView: View {
    @State var isFallowing : Bool = false
    var fallowingData : FallowingModel
    var body: some View {
        VStack (alignment: .leading) {
            HStack (alignment: .center, spacing: 20) {
                Circle()
                    .frame(width: 55, height: 55)
                    .foregroundColor(fallowingData.fallowinngColor)
                
                    VStack (alignment:.leading, spacing: 8){
                        Text(fallowingData.fallowinngName)
                            .font(.subheadline).bold()
                            .foregroundColor(.black)
                        Text(fallowingData.fallowinngNick)
                            .font(.caption)
                            .foregroundColor(.gray)
                    }
                    
                    Spacer()
                Spacer()
                
                Button {
                    isFallowing.toggle()
                } label: {
                    Text(isFallowing ? "Unfallow" : "Fallow")
                        .font(.subheadline)
                        .foregroundColor(isFallowing ? .white : .blue)
                        .frame(width: 70)
                        .padding(.vertical,9)
                        .padding(.horizontal,9)
                        .background(isFallowing ? .blue : .gray.opacity(0.1))
                        .clipShape(Capsule())
                }
            }
            HStack{
                Text(fallowingData.fallowinngDesc)
                    .font(.title3)
                    .foregroundColor(.gray)
            }
            .padding(.leading,5)
            .padding(.top,5)
            Divider()
        }
        .padding()
    }
}

struct FallowingRowView_Previews: PreviewProvider {
    static var previews: some View {
        FallowingRowView(fallowingData: fallowingData.first!)
    }
}
