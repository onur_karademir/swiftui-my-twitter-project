//
//  CreateListView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import SwiftUI

struct CreateListView: View {
    @Environment(\.presentationMode) var presentationMode
    @StateObject var createListViewModelStateObj : CreateListViewModel = CreateListViewModel()
    //let saveButtonText : String
    var body: some View {
        VStack {
            VStack {
                ProfileMenuHeaderView(saveButtonText: "Save", cancelButtonText: "Cancel", headerText: "Create List", stepText: "Step 1/2")
            }
            .padding()
            VStack {
             CreateListHeaderView(createListHeaderImg: "camera")
            }
            VStack {
                ForEach(createListViewModelStateObj.createListViewModel, id:\.id) { item in
                    CreateListTextAreaView(createListKey: item.key, createListValue: item.value)
                }
            }
            Spacer()
        }
    }
}

struct CreateListView_Previews: PreviewProvider {
    static var previews: some View {
        CreateListView()
    }
}
