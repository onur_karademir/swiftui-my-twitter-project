//
//  PlaceMarksView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 15.08.2022.
//

import SwiftUI

struct PlaceMarksView: View {
    @Environment (\.presentationMode) var presentationMode
    @State var openPlaceMarksSheets : Bool = false
    var mainTitle : String
    var subTitle : String
    var buttonText : String
    
    var body: some View {
        ZStack (alignment: .topLeading){
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                BackButtonView(backButtonString: "arrow.left")
            }
            VStack {
                Spacer()
                VStack (spacing: 13){
                    Text(mainTitle)
                        .font(.title2)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.center)
                    Text(subTitle)
                        .foregroundColor(.gray)
                        .font(.subheadline)
                    
                    Button {
                        //actions
                        openPlaceMarksSheets.toggle()
                    } label: {
                        ClasicButtonView(clasicButtonText: buttonText, clasicButtonString: "plus")
                    }

                }
                .padding()
                Spacer()
                Spacer()
                    .navigationBarHidden(true)
            }
            .sheet(isPresented: $openPlaceMarksSheets) {
                CreatePlaceMarksView()
             }
        }
    }
}

struct PlaceMarksView_Previews: PreviewProvider {
    static var previews: some View {
        PlaceMarksView(mainTitle: "You have not created or followed any lists.", subTitle: "When you create it, it will be displayed here.", buttonText: "Create List")
    }
}
