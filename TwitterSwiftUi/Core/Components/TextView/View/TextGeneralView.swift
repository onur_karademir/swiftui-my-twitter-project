//
//  TextGeneralView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct TextGeneralView: View {
    var textString : String
    var body: some View {
        HStack {
            Text(textString)
                .font(.subheadline)
                .fontWeight(.semibold)
                .padding()
        }
        .background(Color.white
            .shadow(color: Color.gray, radius: 10, x: 0, y: 0)
        )
    }
}

struct TextGeneralView_Previews: PreviewProvider {
    static var previews: some View {
        TextGeneralView(textString: "string")
    }
}
