//
//  ProfileMenuHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 23.08.2022.
//

import SwiftUI

struct ProfileMenuHeaderView: View {
    @Environment(\.presentationMode) var presentationMode
    let saveButtonText : String
    let cancelButtonText : String
    let headerText : String
    let stepText : String
    var body: some View {
        HStack {

            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text(cancelButtonText)
                    .foregroundColor(Color(.systemBlue))
                    .bold()
            }
            Spacer()
            VStack{
                Text(headerText)
                    .font(.caption)
                    .foregroundColor(.black).bold()
                Text(stepText)
                    .font(.caption2)
                    .foregroundColor(.gray)
            }
            Spacer()
            Button {
    
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text(saveButtonText)
                    .foregroundColor(Color.white)
                    .bold()
                    .padding(.vertical, 9)
                    .padding(.horizontal)
                    .background(Color(.systemBlue))
                    .clipShape(Capsule())
            }
        }
    }
}

struct ProfileMenuHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileMenuHeaderView(saveButtonText: "Save", cancelButtonText: "Cancel", headerText: "Create Placemark", stepText: "Step 1/1")
    }
}
