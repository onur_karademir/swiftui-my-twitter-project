//
//  ProfileMenuView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 12.08.2022.
//

import SwiftUI

struct ProfileMenuView: View {
    @Environment (\.presentationMode) var presentationMode
    
    @AppStorage("current_user_profile") var currentUserProfile : Bool = false
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("email") var currentUserEmail: String?
    @AppStorage("password") var currentUserPassword: String?
    @AppStorage("user_name") var currentUserName: String?
    @AppStorage("user_full_name") var currentUserFullName: String?
    
    var fData : FallowersModel
    
    var body: some View {
        VStack (alignment: .leading, spacing: 35){
            
            VStack (alignment: .leading){
                HStack {
                    Circle()
                        .frame(width: 50, height: 50)
                    Spacer()
                    VStack {
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Image(systemName: "xmark")
                                .padding()
                                .foregroundColor(Color.black)
                                .font(.subheadline)
                        }
                        
                    }
                }
                
                VStack (alignment: .leading , spacing: 5) {
                    Text(currentUserFullName ?? "")
                        .font(.headline)
                    Text(currentUserName ?? "")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
                
                HStack (spacing: 25){
                    HStack (spacing: 5) {
                        NavigationLink {
                            FallowingView()
                                .navigationBarTitleDisplayMode(.inline)
                                .navigationTitle("Fallowing")
                        } label: {
                            Text("2")
                                .fontWeight(.bold)
                                .foregroundColor(.black)
                            Text("Fallowing")
                                .font(.caption)
                                .foregroundColor(Color.gray)
                        }

                        
                    }
                    HStack{
                        NavigationLink {
                            FallowersView(fData: fData)
                                .navigationBarTitleDisplayMode(.inline)
                                .navigationTitle("Fallowers")
                        } label: {
                            Text("6.9M")
                                .foregroundColor(.black)
                                .fontWeight(.bold)
                        }
                        Text("Fallowers")
                            .font(.caption)
                            .foregroundColor(Color.gray)
                    }
                }
                .padding(.vertical, 2)
            }
            .padding(.leading)
            
            ForEach(SideMenuViewModel.allCases, id:\.rawValue) { item in
                HStack {
                    Image(systemName: item.imageName)
                        .font(.headline)
                        .foregroundColor(.gray)
                    
                    if item == .profile {
                        NavigationLink {
                            
                            /*CurrentUserProfileView(data: dataUser.first!,fData: fallowersData.first!)
                                .navigationBarHidden(true)
                             */
                            
                            /* eski hali asagida*/
                            ProfileView(data: dataUser.first!,fData: fallowersData.first!)
                                .navigationBarHidden(true)
                             
                        } label: {
                            Text(item.title)
                                .font(.subheadline).bold()
                                .foregroundColor(.black)
                        }
                        .simultaneousGesture(
                            TapGesture().onEnded{
                                currentUserProfile = true
                        })
                    }else if item == .lists {
                        NavigationLink {
                            ListsView()
                        } label: {
                            Text(item.title)
                                .font(.subheadline).bold()
                                .foregroundColor(.black)
                        }
                    }else if item == .bookmarks {
                        NavigationLink {
                            PlaceMarksView(mainTitle: "You have not created or followed any lists.", subTitle: "When you create it, it will be displayed here.", buttonText: "Create List")
                        } label: {
                            Text(item.title)
                                .font(.subheadline).bold()
                                .foregroundColor(.black)
                        }
                    }else {
                        Text(item.title)
                            .font(.subheadline).bold()
                            .onTapGesture {
                                signOut()
                            }
                    }
                    
                    
                    Spacer()
                }
                .frame(height: 40)
                .padding(.horizontal)
            }
            Spacer()
        }
    }
    
    func signOut() {
        currentUserSignedIn = false
    }
}

struct ProfileMenuView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileMenuView(fData: fallowersData.first!)
    }
}
