//
//  AuthButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 16.08.2022.
//

import SwiftUI

struct AuthButtonView: View {
    let buttonString : String
    var body: some View {
        Text(buttonString)
            .foregroundColor(.white)
            .frame(width: 340, height: 50)
            .background(.blue)
            .clipShape(Capsule())
            .padding()
    }
}

struct AuthButtonView_Previews: PreviewProvider {
    static var previews: some View {
        AuthButtonView(buttonString: "Sign In")
    }
}
