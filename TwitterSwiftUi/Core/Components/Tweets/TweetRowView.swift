//
//  TweetRowView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct TweetRowView: View {
    var data : userDataModel
    @State var isLike : Bool = false
    @State var isRetweet : Bool = false
    @State var isBookmark : Bool = false
    @State var openSheets: Bool = false
    var body: some View {
        VStack (alignment: .leading){
            //profile image and info
            HStack (alignment: .top, spacing: 12){
                //user image container
                Circle()
                    .frame(width: 56, height: 56)
                    .foregroundColor(data.userColor)
                
                VStack (alignment: .leading, spacing: 4){
                    //user info and user name
                    HStack {
                        Text(data.userName)
                            .font(.headline).bold()
                        Text(data.userNick)
                            .foregroundColor(.gray)
                            .font(.caption)
                        Text("2w")
                            .foregroundColor(.gray)
                            .font(.caption)
                    }
                    //tweet
                    Text("Ben geceleri esen teroörüm.")
                        .font(.subheadline)
                        .multilineTextAlignment(.leading)
                }
                
            }
            //action buttons
            
            HStack {
                Button {
                    openSheets.toggle()
                } label: {
                    Image(systemName: "bubble.left")
                        .font(.subheadline)
                }
                Spacer()
                Button {
                    isRetweet.toggle()
                } label: {
                    Image(systemName: "arrow.2.squarepath")
                        .font(.subheadline)
                        .foregroundColor(isRetweet ? .blue : .gray)
                }
                Spacer()
                Button {
                    isLike.toggle()
                } label: {
                    Image(systemName: isLike ? "heart.fill" : "heart")
                        .font(.subheadline)
                        .foregroundColor(isLike ? .red : .gray)
                }
                Spacer()
                Button {
                    isBookmark.toggle()
                } label: {
                    Image(systemName: isBookmark ? "bookmark.fill" : "bookmark")
                        .font(.subheadline)
                        .foregroundColor(isBookmark ? .blue : .gray)
                }

            }
            .padding()
            .foregroundColor(.gray)
            Divider()
            //fullScreenCover (tam ekran sheets)
                .sheet(isPresented: $openSheets) {
                    NewTweetView()
                 }
        }
        .padding()
    }
}

struct TweetRowView_Previews: PreviewProvider {
    static var previews: some View {
        TweetRowView(data: dataUser.first!)
    }
}
