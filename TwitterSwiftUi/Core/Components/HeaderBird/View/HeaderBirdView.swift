//
//  HeaderBirdView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct HeaderBirdView: View {
    let headerPic : String
    var body: some View {
        Circle()
            .fill(Color.blue)
            .frame(width: 45, height: 45, alignment: .center)
            .overlay(
            Image(headerPic)
                .resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .scaledToFit()
            )
    }
}

struct HeaderBirdView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderBirdView(headerPic: "bird")
    }
}
