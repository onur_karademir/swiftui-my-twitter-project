//
//  AuthenticationHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 15.08.2022.
//

import SwiftUI

struct AuthenticationHeaderView: View {
    var titleText : String
    var subtitleText : String
    var image : String
    
    var body: some View {
        VStack (alignment: .leading, spacing: 5) {
            //2 hstack icinde spacer
            HStack{Spacer()}
            Text(titleText)
                .font(.largeTitle)
                .fontWeight(.semibold)
            HStack (spacing: 13){
                Text(subtitleText)
                    .font(.largeTitle)
                .fontWeight(.semibold)
                Image(image)
                    .resizable()
                    //.scaledToFit()
                    .scaledToFill()
                    .frame(width: 50, height: 0)
            }
        }
        .frame(height: 260)
        .padding(.leading)
        //ekrana tamamen yaymak icin iki pratik yol var.
        //1 frame icine uiscreen kullnamak
        //.frame(width: UIScreen.main.bounds.width, height: 260)
        .background(.blue)
        .foregroundColor(.white)
        .cornerRadius(40)
    }
}

struct AuthenticationHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationHeaderView(titleText: "Hello.", subtitleText: "Welcome Back", image: "bird")
    }
}
