//
//  CreateListTextAreaView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import SwiftUI

struct CreateListTextAreaView: View {
    var createListKey : String
    var createListValue : String
    var body: some View {
        VStack {
            Divider()
            HStack {
                VStack (alignment: .leading, spacing: 10){
                    Text(createListKey)
                        .font(.headline)
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    
                    Text(createListValue)
                        .font(.headline)
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                }
                .padding()
                Spacer()
            }
            Divider()
        }
    }
}

struct CreateListTextAreaView_Previews: PreviewProvider {
    static var previews: some View {
        CreateListTextAreaView(createListKey: "key", createListValue: "value")
    }
}
