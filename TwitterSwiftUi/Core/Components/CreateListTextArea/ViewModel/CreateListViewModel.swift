//
//  CreateListViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import Foundation

struct CreateListTypeModel : Identifiable {
    var id = UUID().uuidString
    var key : String
    var value : String
}

class CreateListViewModel : ObservableObject {
    
    @Published var createListViewModel : [CreateListTypeModel] = []
    
    init() {
        getItems()
    }
    
    func getItems() {
        let item1 = CreateListTypeModel(key: "Name", value: "Add Name")
        let item2 = CreateListTypeModel(key: "Desc", value: "Add here")
        let item3 = CreateListTypeModel(key: "Location", value: "Loction add here")
        let item4 = CreateListTypeModel(key: "Web Site", value: "Web site add here")
        
        createListViewModel.append(item1)
        createListViewModel.append(item2)
        createListViewModel.append(item3)
        createListViewModel.append(item4)
        
    }
}

