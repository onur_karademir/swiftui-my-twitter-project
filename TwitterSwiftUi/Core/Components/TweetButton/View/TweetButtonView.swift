//
//  TweetButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct TweetButtonView: View {
    let tweetButtonName : String
    var body: some View {
        Image(systemName: tweetButtonName)
            .resizable()
            .renderingMode(.template)
            .frame(width: 33, height: 33)
            .padding()
    }
}

struct TweetButtonView_Previews: PreviewProvider {
    static var previews: some View {
        TweetButtonView(tweetButtonName: "highlighter")
    }
}
