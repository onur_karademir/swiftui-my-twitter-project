//
//  NotifBodyView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct NotifBodyView: View {
    let personNotifName : String
    let personNotifDetail : String
    let personNotif : String
    var body: some View {
        VStack (alignment: .leading){
            HStack (alignment: .top){
                Text(personNotifName)
                    .font(.subheadline)
                    .fontWeight(.bold)
                Text(personNotifDetail).bold()
                    .foregroundColor(.gray)
                    .font(.caption)
                    .lineLimit(5)
                    .minimumScaleFactor(0.9)
            }
            .frame(height: 30)
            Text(personNotif)
                .font(.subheadline)
                .fontWeight(.regular)
        }
    }
}

struct NotifBodyView_Previews: PreviewProvider {
    static var previews: some View {
        NotifBodyView(personNotifName: "name", personNotifDetail: "detail", personNotif: "notif")
    }
}
