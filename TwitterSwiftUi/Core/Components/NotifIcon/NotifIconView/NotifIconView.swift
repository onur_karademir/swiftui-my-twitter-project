//
//  NotifIconView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct NotifIconView: View {
    let iconName : String
    let iconImageColor : Color
    let userIconColor : Color
    var body: some View {
        HStack (spacing: 33){
            Image(systemName: iconName)
                .foregroundColor(iconImageColor)
            Circle()
                .frame(width: 55, height: 55)
            .foregroundColor(userIconColor)
        }
    }
}

struct NotifIconView_Previews: PreviewProvider {
    static var previews: some View {
        NotifIconView(iconName: "star.fill", iconImageColor: .red, userIconColor: .red)
    }
}
