//
//  AuthFooterButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 16.08.2022.
//

import SwiftUI

struct AuthFooterButtonView: View {
    let textOne : String
    let textTwo : String
    var body: some View {
        HStack {
            Text(textOne)
                .font(.subheadline)
                .fontWeight(.bold)
            Text(textTwo)
                .font(.subheadline)
                .fontWeight(.bold)
        }
    }
}

struct AuthFooterButtonView_Previews: PreviewProvider {
    static var previews: some View {
        AuthFooterButtonView(textOne: "Don't have a accont?", textTwo: "Sign Up")
    }
}
