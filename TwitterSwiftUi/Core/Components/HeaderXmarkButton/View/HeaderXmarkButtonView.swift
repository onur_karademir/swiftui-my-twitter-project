//
//  HeaderXmarkButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct HeaderXmarkButtonView: View {
    let headerButtonString : String
    var body: some View {
        Image(systemName: headerButtonString)
            .padding(.top)
            .foregroundColor(Color.black)
            .font(.subheadline)
    }
}

struct HeaderXmarkButtonView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderXmarkButtonView(headerButtonString: "xmark")
    }
}
