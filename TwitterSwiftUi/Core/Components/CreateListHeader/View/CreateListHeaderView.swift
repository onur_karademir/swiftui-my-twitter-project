//
//  CreateListHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import SwiftUI

struct CreateListHeaderView: View {
    let createListHeaderImg : String
    var body: some View {
        VStack {
            HStack{Spacer()}
            Image(systemName: createListHeaderImg)
                .foregroundColor(.white)
                .font(.largeTitle)
        }
        .frame(height: 140, alignment: .center)
        .background(.gray).opacity(0.5)
    }
}

struct CreateListHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        CreateListHeaderView(createListHeaderImg: "camera")
    }
}
