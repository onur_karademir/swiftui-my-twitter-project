//
//  NotifMenuView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 14.08.2022.
//

import SwiftUI

struct NotifMenuView: View {
    @State var selectefNotifFilter : NotifMenuViewModel = .all
    var body: some View {
        VStack {
            HStack {
                ForEach(NotifMenuViewModel.allCases, id:\.rawValue) { item in
                    VStack {
                        Text(item.title)
                            .fontWeight(selectefNotifFilter == item ? .semibold : .regular)
                        .foregroundColor(selectefNotifFilter == item ? .black : .gray)
                        if selectefNotifFilter == item {
                            Capsule()
                                .foregroundColor(.blue)
                                .frame(height:3)
                        }else {
                            Capsule()
                                .foregroundColor(.clear)
                                .frame(height:3)
                        }
                    }
                    .onTapGesture {
                        withAnimation (.easeInOut){
                            self.selectefNotifFilter = item
                        }
                    }
                }
            }
        }
    }
}

struct NotifMenuView_Previews: PreviewProvider {
    static var previews: some View {
        NotifMenuView()
    }
}
