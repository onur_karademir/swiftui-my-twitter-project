//
//  NotifMenuViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 14.08.2022.
//

import Foundation

enum NotifMenuViewModel : Int, CaseIterable {
    case all
    case notification
    
    var title : String {
        switch self {
            
        case .all : return "All"
        case.notification : return "Notification"
            
        }
    }
}
