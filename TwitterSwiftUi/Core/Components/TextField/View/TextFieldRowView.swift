//
//  TextFieldRowView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 15.08.2022.
//

import SwiftUI

struct TextFieldRowView: View {
    let placeholder : String
    @Binding var bindingText : String
    
    var body: some View {
        TextField(placeholder, text: $bindingText)
    }
}

struct TextFieldRowView_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldRowView(placeholder: "Email", bindingText: .constant(""))
    }
}
