//
//  BackButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct BackButtonView: View {
    let backButtonString : String
    var body: some View {
        Image(systemName: backButtonString)
            .foregroundColor(.white)
            .font(.headline)
            .padding(.horizontal)
            .padding(.vertical, 8)
            .background(Color.blue)
            .clipShape(Capsule())
    }
}

struct BackButtonView_Previews: PreviewProvider {
    static var previews: some View {
        BackButtonView(backButtonString: "arrow.left")
    }
}
