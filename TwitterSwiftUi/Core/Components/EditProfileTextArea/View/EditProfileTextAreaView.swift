//
//  EditProfileTextAreaView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 21.08.2022.
//

import SwiftUI

struct EditProfileTextAreaView: View {
    
    var key : String
    var value : String
    
    var body: some View {
        VStack {
            Divider()
            HStack {
                HStack (alignment: .center, spacing: 30){
                    Text(key)
                        .font(.headline)
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    
                    Text(value)
                        .font(.headline)
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    Spacer()
                }
                .padding()
                
            }
            Divider()
        }
    }
}

struct EditProfileTextAreaView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileTextAreaView(key: "key", value: "value")
    }
}
