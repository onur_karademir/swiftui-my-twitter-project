//
//  EditProfileViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 21.08.2022.
//

import Foundation


struct EditProfileTypeModel : Identifiable {
    var id = UUID().uuidString
    var key : String
    var value : String
}

class EditProfileViewModel : ObservableObject {
    
    @Published var editProfileViewModel : [EditProfileTypeModel] = []
    
    init() {
        getItems()
    }
    
    func getItems() {
        let item1 = EditProfileTypeModel(key: "Name", value: "Bruce Wayne")
        let item2 = EditProfileTypeModel(key: "Personal info", value: "Add here")
        let item3 = EditProfileTypeModel(key: "Location", value: "Loction add here")
        let item4 = EditProfileTypeModel(key: "Web Site", value: "Web site add here")
        let item5 = EditProfileTypeModel(key: "Birth Day", value: "Add here")
        
        editProfileViewModel.append(item1)
        editProfileViewModel.append(item2)
        editProfileViewModel.append(item3)
        editProfileViewModel.append(item4)
        editProfileViewModel.append(item5)
        
    }
}
