//
//  EditProfileHeaderView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 20.08.2022.
//

import SwiftUI

struct EditProfileHeaderView: View {
    var headerString : String
    var headerImage : String
    var body: some View {
        ZStack (alignment: .bottomLeading){
            Circle()
                .frame(width: 60, height: 60)
                .offset(x: 0, y: 20)
                .overlay(
                    Image(systemName: "camera.fill")
                        .foregroundColor(.white)
                        .offset(x: 0, y: 20)
                )
            VStack {
                HStack{Spacer()}
                    VStack {
                        Text(headerString)
                            .font(.title)
                            .foregroundColor(.white)
                        .fontWeight(.bold)
                        Image(headerImage)
                            .resizable()
                            .frame(width: 55, height: 55)
                    }
                VStack{
                    Text("image area...")
                        .foregroundColor(.white)
                }
                Spacer()
            }
            .frame(height:150)
        .padding()
        }
        .background(Color(.systemBlue))
       
    }
}

struct EditProfileHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileHeaderView(headerString: "Profile Edit.", headerImage: "bird")
    }
}
