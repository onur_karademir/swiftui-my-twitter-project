//
//  ClasicButtonView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 29.08.2022.
//

import SwiftUI

struct ClasicButtonView: View {
    let clasicButtonText : String
    let clasicButtonString : String
    var body: some View {
        HStack {
            Text(clasicButtonText)
            Image(systemName: clasicButtonString)
        }
        .foregroundColor(.white)
        .font(.headline)
        .padding(.horizontal)
        .padding(.vertical, 10)
        .background(Color.blue)
        .clipShape(Capsule())
    }
}

struct ClasicButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ClasicButtonView(clasicButtonText: "Info", clasicButtonString: "info")
    }
}
