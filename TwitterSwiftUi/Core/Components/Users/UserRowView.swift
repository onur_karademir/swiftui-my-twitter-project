//
//  UserRowVIew.swift
//  TwitterSwiftUi
//
//  Created by Onur on 11.08.2022.
//

import SwiftUI

struct UserRowView: View {
    
    var userNick : String
    var userName : String
    var userColor : Color
    
    var body: some View {
        HStack (spacing: 12){
            Circle()
                .frame(width: 45, height: 45)
                .foregroundColor(userColor)
            
            VStack (alignment: .leading, spacing: 5){
                Text(userName)
                    .font(.subheadline).bold()
                    .foregroundColor(.black)
                Text(userNick)
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 5)
    }
}

struct UserRowView_Previews: PreviewProvider {
    static var previews: some View {
        UserRowView(userNick: "Joker", userName: "Heath Ledger", userColor: .blue)
    }
}
