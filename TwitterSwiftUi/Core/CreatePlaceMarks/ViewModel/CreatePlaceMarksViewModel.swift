//
//  CreatePlaceMarksViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import Foundation


struct CreatePlaceMarksTypeModel : Identifiable {
    let id = UUID()
    var placeMarkKey : String
    var placeMarkValue : String
}

class CreatePlaceMarksViewModel : ObservableObject {
    
    @Published var createPlaceMarksViewModel : [CreatePlaceMarksTypeModel] = []
    
    init () {
        getItems()
    }
    
    func getItems () {
        let item_one = CreatePlaceMarksTypeModel(placeMarkKey: "Loction", placeMarkValue: "Loction add here")
        let item_two = CreatePlaceMarksTypeModel(placeMarkKey: "Name", placeMarkValue: "Name here")
        
        createPlaceMarksViewModel.append(item_one)
        createPlaceMarksViewModel.append(item_two)
    }
}
