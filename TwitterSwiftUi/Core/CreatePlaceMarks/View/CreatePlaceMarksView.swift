//
//  CreatePlaceMarksView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 22.08.2022.
//

import SwiftUI

struct CreatePlaceMarksView: View {
    @Environment(\.presentationMode) var presentationMode
    @StateObject var createPlaceMarksViewModelStateObj : CreatePlaceMarksViewModel = CreatePlaceMarksViewModel()
    //let saveButtonText : String
    var body: some View {
        VStack {
            VStack {
                ProfileMenuHeaderView(saveButtonText: "Save", cancelButtonText: "Cancel", headerText: "Create Bookmark", stepText: "Step 1/1")
            }
            .padding()
            VStack {
             CreateListHeaderView(createListHeaderImg: "pin")
            }
            VStack {
                ForEach(createPlaceMarksViewModelStateObj.createPlaceMarksViewModel, id:\.id) { item in
                    CreateListTextAreaView(createListKey: item.placeMarkKey, createListValue: item.placeMarkValue)
                }
            }
            Spacer()
        }
    }
}

struct CreatePlaceMarksView_Previews: PreviewProvider {
    static var previews: some View {
        CreatePlaceMarksView()
    }
}
