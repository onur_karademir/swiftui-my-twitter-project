//
//  HastagDataViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 17.08.2022.
//

import Foundation


struct HastgDataModel : Identifiable {
    let id : String = UUID().uuidString
    let hastag : String
}

struct HastagList {
    static let topTen = [
        HastgDataModel(hastag: "one"),
        HastgDataModel(hastag: "two")
    ]
    
    static let topFive = [
        HastgDataModel(hastag: "oned"),
        HastgDataModel(hastag: "twod")
    ]
}
