//
//  ExploreViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 13.08.2022.
//

import Foundation
import SwiftUI

struct HastagDetailModel : Identifiable {
    
var id = UUID()
var title: String
var tweetCount : String
var hastagDetail: [String]
var userHastagColor : Color
var userHastagName : String
var userHastagNick : String
var tweetDate : String
var hastagUsers : [String]
    
}

enum ExploreViewModel : Int, CaseIterable {
    case stateone
    case statetwo
    case statethree
    case statefour
    
    var title : String {
        switch self {
        case .stateone : return "#ogretmendiyorki"
        case .statetwo : return "#cumartesi"
        case .statethree : return "#bugungunlerdenfenerbahce"
        case .statefour : return "#bayrak"
        }
    }
    
    var tweetCount : String {
        switch self {
        case .stateone : return "2.899 Tweet"
        case .statetwo : return "5.899 Tweet"
        case .statethree : return "567 Tweet"
        case .statefour : return "4.555 Tweet"
        }
    }
}
