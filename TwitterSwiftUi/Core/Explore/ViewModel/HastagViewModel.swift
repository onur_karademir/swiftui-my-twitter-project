//
//  HastagViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 16.08.2022.
//

import Foundation


struct HastagTypeModel : Identifiable {
    let id : String = UUID().uuidString
    let name : String
}


class HastagViewModel : ObservableObject {
    
    @Published var hastagModel : [HastagTypeModel] = []
    
    init() {
        getItems()
    }
    
    func getItems() {
        let item1 = HastagTypeModel(name: "iphone")
        let item2 = HastagTypeModel(name: "samsung")
        
        hastagModel.append(item1)
        hastagModel.append(item2)
    }
}
