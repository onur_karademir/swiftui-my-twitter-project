//
//  ExploreView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct userDataModel : Identifiable {
    let id = UUID().uuidString
    let userNick : String
    let userName : String
    let userColor : Color
    let userTitle : String
    let userLocation : String
    let userWebSite : String
    let userTweet : String
}

let dataUser = [
    userDataModel(userNick: "@batman", userName: "Bruce Wayne",userColor: .red,userTitle : "Ben geceleri esen terörüm.", userLocation: "Gotham, NY",userWebSite : "www.batman.com",userTweet : "tweet 1"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger",userColor: .green,userTitle : "I hate Batman.", userLocation: "Gotham, NY",userWebSite : "www.dc.com",userTweet : "tweet 2"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart",userColor: .purple,userTitle : "It's not about what I want, it's about what's fair!", userLocation: "Gotham, NY",userWebSite : "www.dc.com",userTweet : "tweet 3"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne",userColor: .mint,userTitle : "Ben geceleri esen terörüm.", userLocation: "Gotham, NY",userWebSite : "www.dc.com",userTweet : "tweet 4"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger",userColor: .black,userTitle : "I hate Batman.", userLocation: "Gotham, NY",userWebSite : "www.dc.com",userTweet : "tweet 5"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart",userColor: .blue,userTitle : "It's not about what I want, it's about what's fair!", userLocation: "Gotham, NY",userWebSite : "www.batman.com",userTweet : "tweet 6"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne",userColor: .yellow,userTitle : "Ben geceleri esen terörüm.", userLocation: "Gotham, NY",userWebSite : "www.batman.com",userTweet : "tweet 6"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger",userColor: .purple,userTitle : "I hate Batman.", userLocation: "Gotham, NY",userWebSite : "www.batman.com",userTweet : "tweet 6"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart",userColor: .green,userTitle : "It's not about what I want, it's about what's fair!", userLocation: "Gotham, NY",userWebSite : "www.dc.com",userTweet : "tweet 6"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne",userColor: .mint,userTitle : "Ben geceleri esen terörüm.", userLocation: "Gotham, NY",userWebSite : "www.batman.com",userTweet : "tweet 6")
]

struct ExploreView: View {
    var body: some View {
            VStack{
                ScrollView {
                    TopPictureView()
                    HastagView()
                    VStack {
                        ForEach(dataUser, id: \.id) { item in
                            NavigationLink {
                                ProfileView(data: item, fData: fallowersData.first!)
                                    .navigationBarHidden(true)
                            } label: {
                                UserRowView(userNick: item.userNick, userName: item.userName, userColor: item.userColor)
                            }

                        }
                    }
                    
                }
            }

    }
}

struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}

struct TopPictureView: View {
    var body: some View {
        VStack (spacing: 0){
            
            Rectangle()
                .frame(height: 150)
                .foregroundColor(.mint)
                .overlay(Text("Picture zone"))
            Rectangle()
                .frame(height: 16)
                .foregroundColor(.gray)
                .opacity(0.3)
        }
    }
}

struct HastagView: View {
     var hastagArray: [HastagDetailModel] = [
        HastagDetailModel(
            title: "#ogretmendiyorki",
            tweetCount : "2.999",
            hastagDetail: ["tweet1 #ogretmendiyorki", "tweet2 #ogretmendiyorki", "tweet3 #ogretmendiyorki","tweet4 #ogretmendiyorki","tweet5 #ogretmendiyorki"],
            userHastagColor: .mint,
            userHastagName: "Tom Cruize",
            userHastagNick: "@mission_imp",
            tweetDate: "2w",
            hastagUsers: ["Tom Cruize","Capt. Jack Sparrow","Jhon Lock","Sawyer1"]),
        
        HastagDetailModel(
            title: "#cumartesi",
            tweetCount : "2.787",
            hastagDetail: ["tweet1 #cumartesi", "tweet2 #cumartesi", "tweet3 #cumartesi","tweet4 #cumartesi","tweet5 #cumartesi"],
            userHastagColor: .blue,
            userHastagName: "Capt. Jack Sparrow",
            userHastagNick: "@capt",
            tweetDate: "3w",
            hastagUsers: ["Keira Knightley","Tom Cruize","Jhon Lock","Sawyer2"]),
        
        HastagDetailModel(
            title: "#bugungunlerdenfenerbahce",
            tweetCount : "1.200",
            hastagDetail: ["tweet1 #bugungunlerdenfenerbahce", "tweet2 #bugungunlerdenfenerbahce", "tweet3 #bugungunlerdenfenerbahce","tweet4 #bugungunlerdenfenerbahce","tweet5 #bugungunlerdenfenerbahce"],
            userHastagColor: .yellow,
            userHastagName: "Keira Knightley",
            userHastagNick: "@crown",
            tweetDate: "1w",
            hastagUsers: ["Capt. Jack Sparrow","Tom Cruize","Jhon Lock","Sawyer3"]),
        
        HastagDetailModel(
            title: "#bayrak",
            tweetCount : "577",
            hastagDetail: ["tweet1 #bayrak", "tweet2 #bayrak", "tweet3 #bayrak","tweet4 #bayrak","tweet5 #bayrak"],
            userHastagColor: .purple,
            userHastagName: "Better Call Soul",
            userHastagNick: "@soul",
            tweetDate: "4w",
            hastagUsers: ["Tom Cruize","Capt. Jack Sparrow","Jhon Lock","Sawyer4"])
    ]
    
    var body: some View {
        VStack (alignment: .leading) {
            Text("Turkey")
                .font(.title2).bold()
            Divider()
            ForEach(hastagArray, id:\.id) { item in
                VStack (alignment: .leading, spacing: 6) {
                    NavigationLink {
                        HastagDetailPageView(hastag: item.title, name: item.userHastagName, nick: item.userHastagNick, date: item.tweetDate, color: item.userHastagColor, hastagDetail: item)
                    } label: {
                        VStack (alignment: .leading, spacing: 10){
                            Text(item.title).bold()
                                .foregroundColor(.black)
                            Text(item.tweetCount)
                                .font(.subheadline)
                                .foregroundColor(.gray)
                        }
                    }
                }
                Divider()
            }
        }
        .padding()
    }
}

struct HastagDetailPageView: View {
    let hastag: String
    let name : String
    let nick : String
    let date : String
    let color : Color
    var hastagDetail : HastagDetailModel
    
    var body: some View {
        VStack (alignment: .leading){
            HStack {
                Text("hastag")
                    .font(.subheadline)
                Text(hastag)
                    .font(.title3).bold()
            }
            .padding(.horizontal)
            ScrollView {
                LazyVStack (alignment: .leading, spacing: 20) {
                    ForEach(hastagDetail.hastagDetail, id:\.self) { item in
                            HStack (alignment: .top, spacing: 12){
                                Circle()
                                    .frame(width: 56, height: 56)
                                    .foregroundColor(color)
                                
                                VStack (alignment: .leading, spacing: 4){
                                    //user info and user name
                                    HStack (spacing: 20){
                                        Text(name)
                                            .font(.headline).bold()
                                        Text(nick)
                                            .foregroundColor(.gray)
                                            .font(.caption)
                                        Text(date)
                                            .foregroundColor(.gray)
                                            .font(.caption)
                                    }
                                    //tweet
                                    Text(item)
                                        .font(.subheadline)
                                        .multilineTextAlignment(.leading)
                                }
                            }
                    }
//                    ForEach(hastagDetail.hastagUsers, id:\.self) { item in
//                        Text(item)
//                        Text(hastagDetail.title)
//                    }
                }
                .padding()
            }
        }
    }
    
}
