//
//  AuthenticationView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 15.08.2022.
//

import SwiftUI

struct AuthenticationView: View {
    @State var email = ""
    @State var password = ""
    
    // alert
    @State var alertTitle : String = ""
    @State var showAlert : Bool = false
    
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    @AppStorage("email") var currentUserEmail: String?
    @AppStorage("password") var currentUserPassword: String?
    
    var body: some View {
        VStack{
            
            AuthenticationHeaderView(titleText: "Hello.", subtitleText: "Welcome Back", image: "bird")
            
            VStack (spacing: 40){
                TextFieldRowView(placeholder: "Email", bindingText: $email)
                    .keyboardType(.emailAddress)
                    .textInputAutocapitalization(.never)
                SecureField( "Password", text: $password)
                //eskisi asagida
//                TextFieldRowView(placeholder: "Password", bindingText: $password)
            }
            .padding(.horizontal, 32)
            .padding(.top, 44)
            
            Button {
                signIn()
            } label: {
                AuthButtonView(buttonString: "Sign In")
            }
            .shadow(color: .gray, radius: 1, x:0 , y: 0)

            Spacer()
            
            NavigationLink {
                RegisterView()
                    .navigationBarHidden(true)
            } label: {
                AuthFooterButtonView(textOne: "Don't have a accont?", textTwo: "Sign Up")
            }
            .foregroundColor(.blue)
            .padding()
            .padding(.bottom)
            .alert(isPresented: $showAlert) {
                return Alert(title: Text(alertTitle))
            }
        }
        .ignoresSafeArea()
    }
    func signIn() {
        if email == currentUserEmail && password == currentUserPassword {
            currentUserSignedIn = true
        } else {
            alertTitle = "There is something wrong, you should check again.🥸"
            showAlert.toggle()
        }
    }
}

struct AuthenticationView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationView()
    }
}
