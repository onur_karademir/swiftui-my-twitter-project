//
//  RegisterView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 15.08.2022.
//

import SwiftUI

struct RegisterView: View {
    @State var email = ""
    @State var password = ""
    @State var username = ""
    @State var fullname = ""
    
    // alert
    @State var alertRegisterTitle : String = ""
    @State var showAlertRegister : Bool = false
    
    //appstorage data
    
    @AppStorage("email") var currentUserEmail: String?
    @AppStorage("password") var currentUserPassword: String?
    @AppStorage("user_name") var currentUserName: String?
    @AppStorage("user_full_name") var currentUserFullName: String?
    
    var body: some View {
        VStack{
            //bunu bilerek biraktim componenti var auth view de kullandim.
            VStack (alignment: .leading, spacing: 5) {
                //2 hstack icinde spacer kullanmak
                HStack{Spacer()}
                Text("Get Started.")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                HStack (spacing: 13){
                    Text("Create Accont")
                        .font(.largeTitle)
                    .fontWeight(.semibold)
                    Image("bird")
                        .resizable()
                        //.scaledToFit()
                        .scaledToFill()
                        .frame(width: 50, height: 0)
                }
            }
            .frame(height: 260)
            .padding(.leading)
            //ekrana tamamen yaymak icin iki pratik yol var.
            //1 frame icine uiscreen kullnamak
            //.frame(width: UIScreen.main.bounds.width, height: 260)
            .background(.blue)
            .foregroundColor(.white)
            .cornerRadius(40)
            //
        VStack (spacing: 40){
            TextFieldRowView(placeholder: "Email", bindingText: $email)
                .keyboardType(.emailAddress)
                .textInputAutocapitalization(.never)
            TextFieldRowView(placeholder: "User Name", bindingText: $username)
            TextFieldRowView(placeholder: "Full Name", bindingText: $fullname)
//            TextFieldRowView(placeholder: "Password", bindingText: $password)
            SecureField( "Password", text: $password)
        }
            .padding(.horizontal, 32)
            .padding(.top, 44)
            
            Button {
                //actions
                signUp()
                
            } label: {
                AuthButtonView(buttonString: "Sign Up")
            }
            .shadow(color: .gray, radius: 1, x:0 , y: 0)

            Spacer()
            
            NavigationLink {
                AuthenticationView()
                    .navigationBarHidden(true)
            } label: {
                AuthFooterButtonView(textOne: "You have a accont?", textTwo: "Sign In")
            }
            .foregroundColor(.blue)
            .padding()
            .padding(.bottom)
            .alert(isPresented: $showAlertRegister) {
                return Alert(title: Text(alertRegisterTitle))
            }
        }
        .ignoresSafeArea()
    }
    func signUp() {
        if email.count >= 3 && password.count >= 3 && username.count >= 3 && fullname.count >= 3{
            guard email.contains("@") else {
                alertRegisterTitle = "Are you sure it's an email address? 🧐"
                showAlertRegister.toggle()
                return
            }
            currentUserEmail = email
            currentUserPassword = password
            currentUserName = username
            currentUserFullName = fullname
            
            alertRegisterTitle = "Congratulations go to Sign in page down here 🥰 👇🏻 🎉"
            showAlertRegister.toggle()
            
        } else {
            alertRegisterTitle = "All fields must be at least 3 characters long! 🧐"
            showAlertRegister.toggle()
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
