//
//  MessagesViewModel.swift
//  TwitterSwiftUi
//
//  Created by Onur on 13.08.2022.
//

import Foundation
import SwiftUI


enum MessagesViewModel : Int, CaseIterable {
    case msgOne
    case msgTwo
    case msgThree
    
    
    var person : String {
        
        switch self {
        case .msgOne: return "Tom Cruize"
        case .msgTwo: return "Capt. Jack Sparrow"
        case .msgThree: return "Keira Knightley"
        }
    }
    
    var message : String {
        
        switch self {
        case .msgOne: return "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        case .msgTwo: return "type specimen book. It has survived not only five centuries, but also the leap"
        case .msgThree: return "tly with desktop publishing software like Aldus PageMaker including versions"
        }
    }
    var nick : String {
        
        switch self {
        case .msgOne: return "@mission_imp"
        case .msgTwo: return "@capt"
        case .msgThree: return "@crown"
        }
    }
    
    var date : String {
        
        switch self {
        case .msgOne: return "01.02.2021"
        case .msgTwo: return "02.03.2019"
        case .msgThree: return "05.07.2022"
        }
    }
    
    var msgUserColor : Color {
        
        switch self {
        case .msgOne: return .purple
        case .msgTwo: return .yellow
        case .msgThree: return .green
        }
    }

}
