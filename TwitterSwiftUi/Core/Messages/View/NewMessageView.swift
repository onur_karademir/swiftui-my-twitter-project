//
//  NewMessageView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 16.08.2022.
//

import SwiftUI

struct NewMessageView: View {
    @State var messageTextFieldData: String = ""
    @State var messageTextFieldTweet : String = ""
    @FocusState private var messageIsFocused : Bool
    let msgPlaceHolder : String
    let msgSendButton : String
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            newMessageHeader
            newMessageBody
            VStack {
                Text(messageTextFieldTweet)
                    .frame(height: 100)
            }
            .padding()
            Spacer()
        }
        .padding()
    }

}

struct NewMessageView_Previews: PreviewProvider {
    static var previews: some View {
        NewMessageView(msgPlaceHolder: "Message Type here...", msgSendButton: "Send")
    }
}


extension NewMessageView {
    var newMessageHeader : some View {
        
        HStack {
            
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Cancel")
                    .foregroundColor(Color(.systemBlue))
                    .bold()
            }
            Spacer()
            
            Button {
                messageIsFocused = true
                messageTextFieldTweet = messageTextFieldData
                messageTextFieldData = ""
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text(msgSendButton)
                    .foregroundColor(Color.white)
                    .bold()
                    .padding(.vertical, 9)
                    .padding(.horizontal)
                    .background(Color(.systemBlue))
                    .clipShape(Capsule())
            }
        }

    }
    var newMessageBody : some View {
        HStack {
            Circle()
                .frame(width: 50, height: 50)
            TextField(msgPlaceHolder, text: $messageTextFieldData)
                .focused($messageIsFocused)
                .padding()
                .font(.headline)
                .foregroundColor(.black)
                .background(Color.white)
                .cornerRadius(50)
            .border(Color.secondary, width: 0.5)
        }
        .padding(.vertical,20)
    }
}
