//
//  MessagesView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct MessagesView: View {
    @State var openMsgSheets: Bool = false
    var body: some View {
            ZStack (alignment: .bottomTrailing){

                VStack (alignment: .leading){
                    ScrollView {
                        LazyVStack (alignment: .leading, spacing: 30){
//                            HStack {
//                                Spacer()
//                                Text("Messages")
//                                    .font(.largeTitle).bold()
//                                    .multilineTextAlignment(.center)
//                                Spacer()
//                            }
                            ForEach(MessagesViewModel.allCases, id:\.rawValue) { item in
                                NavigationLink {
                                    messageDetailView(messageDetailText: item.message, messageDetailPerson: item.person, messageDetailNick: item.nick, messageDetailDate: item.date, messageDetailColor: item.msgUserColor)
                                } label: {
                                    HStack (spacing: 10){
                                        Circle()
                                            .frame(width: 49, height: 49)
                                            .foregroundColor(item.msgUserColor)
                                        
                                        VStack (alignment: .leading, spacing: 8){
                                            HStack {
                                                Text(item.person)
                                                    .font(.subheadline).bold()
                                                Text(item.nick)
                                                    .font(.caption)
                                                    .foregroundColor(.gray)
                                                Spacer()
                                                Text(item.date)
                                                    .font(.subheadline)
                                                    .foregroundColor(.gray)
                                            }
                                            Text(item.message)
                                                .font(.subheadline)
                                                .foregroundColor(.gray)
                                                .multilineTextAlignment(.leading)
                                        }
                                    }
                                    .foregroundColor(.black)
                                }
                                Divider()
                            }
                        }
                    }
                }
                .padding()
                Button {
                    openMsgSheets.toggle()
                } label: {
                    TweetButtonView(tweetButtonName: "arrow.up.message.fill")
                }
                .background(Color.blue)
                .clipShape(Circle())
                .foregroundColor(.white)
                .padding()
                .sheet(isPresented: $openMsgSheets) {
                    NewMessageView(msgPlaceHolder: "Message Type here...", msgSendButton: "Send")
                 }
            }
    }
}

struct MessagesView_Previews: PreviewProvider {
    static var previews: some View {
        MessagesView()
    }
}


struct messageDetailView: View {
    var messageDetailText : String
    var messageDetailPerson : String
    var messageDetailNick : String
    var messageDetailDate : String
    var messageDetailColor : Color
    var body: some View {
        VStack {
            VStack {
                HStack {
                    Text("Message Detail").bold()
                        .font(.title)
                    Spacer()
                    Image("bird")
                        .resizable()
                        .frame(width: 33, height: 33)
                }
            }
            HStack (spacing: 10){
                Circle()
                    .frame(width: 49, height: 49)
                    .foregroundColor(messageDetailColor)
                
                VStack (alignment: .leading, spacing: 8){
                    HStack {
                        Text(messageDetailPerson)
                            .font(.subheadline).bold()
                        Text(messageDetailNick)
                            .font(.caption)
                            .foregroundColor(.gray)
                        Spacer()
                        Text(messageDetailDate)
                            .font(.subheadline)
                            .foregroundColor(.gray)
                    }
                    Text(messageDetailText)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                        .multilineTextAlignment(.leading)
                }
            }
            HStack{
                HStack{Spacer()}
            }
            .frame(height: 2)
            .background(.gray.opacity(0.2))
            .shadow(color: .mint, radius: 1, x: 0, y: 0)
            Spacer()
        }
        .padding()
        Divider()
    }
}
