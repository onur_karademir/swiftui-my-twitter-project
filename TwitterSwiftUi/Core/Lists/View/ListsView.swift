//
//  ListsView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 14.08.2022.
//

import SwiftUI

struct ListsView: View {
    @Environment (\.presentationMode) var presentationMode
    @State var openListSheets : Bool = false
    var body: some View {
        ZStack (alignment: .topLeading){
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                BackButtonView(backButtonString: "arrow.left")
            }
            VStack {
                Spacer()
                VStack (spacing: 13){
                    Text("You have not created or viewed any lists.")
                        .font(.title2)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.center)
                    Text("When you create it, it will be displayed here.")
                        .foregroundColor(.gray)
                        .font(.subheadline)
                    
                    Button {
                        //actions
                        openListSheets.toggle()
                    } label: {
                        ClasicButtonView(clasicButtonText: "Create List", clasicButtonString: "plus")
                        }

                }
                .padding()
                Spacer()
                Spacer()
                    .navigationBarHidden(true)
            }
            .sheet(isPresented: $openListSheets) {
                CreateListView()
             }
        }
    }
}

struct ListsView_Previews: PreviewProvider {
    static var previews: some View {
        ListsView()
    }
}
