//
//  TwitterSwiftUiApp.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

@main
struct TwitterSwiftUiApp: App {
    @AppStorage("signed_in") var currentUserSignedIn : Bool = false
    var body: some Scene {
        WindowGroup {
            NavigationView {
                if currentUserSignedIn {
                    ContentView()
                }else {
                    AuthenticationView()
                }
            }
        }
    }
}
